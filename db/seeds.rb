# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminRole.create!(name: 'super_admin')
AdminRole.create!(name: 'admin')

AdminUser.create!(
	email: 'admin@example.com',
	password: ENV['ADMIN_USER_PASSWORD'],
	password_confirmation: ENV['ADMIN_USER_PASSWORD'],
	admin_role_id: AdminRole.find_by(name: 'super_admin').id
) if Rails.env.development?