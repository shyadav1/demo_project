ActiveAdmin.register User do

  permit_params :email, :password, :password_confirmation
  actions :all, except: [:destroy]

  # Columns present on the Blog page.
  index do
    column :email
    column :confirmation_token
    column :created_at
    column :blogs do |resource|
      resource.blogs.count
    end
    column :create_blog do |resource|
      link_to('Create', new_admin_blog_path(blog: { user_id: resource.id }))
    end
    actions
  end

  # Filters attributes on the Blog page.
  filter :blogs
  filter :created_at
  filter :unconfirmed_email

  # Form inputs fields to create User.
  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  controller do

    # Returns the list of User which are registered using confirmation token.
    def scoped_collection
      super.where.not(confirmed_at: nil)
    end

  end
  
end
