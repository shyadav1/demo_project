ActiveAdmin.register Blog do

  permit_params :title, :body, :user_id

  # Columns present on the Blog page.
  index do
    column :title
    column :body
    column :user
  end

  # Filter attributes for the Blog page.
  filter :user, as: :check_boxes, collection: proc { User.pluck(:email) }
  filter :title
  filter :body

  # Forms input to create the Blog.
  form do |f|
    f.inputs do
      f.input :user_id, input_html: { readonly: true } do
        params[:user_id]
      end
      f.input :title
      f.input :body
    end
    f.actions
  end
  
end
