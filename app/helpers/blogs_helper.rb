module BlogsHelper

  def form_url(blog)
    params[:action] == 'new' ? user_blogs_path(current_user) : user_blog_path(current_user, @blog)
  end

end
