class ApplicationController < ActionController::Base

  # Provides the path to redirect after sign in.
  #
  # @params resource [Object<User]
  def after_sign_in_path_for(resource)
    if resource.is_a?(AdminUser)
      super
    else
      user_path(resource) || root_path
    end
  end

end
