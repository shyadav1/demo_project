class HomeController < ApplicationController

  # Renders the home page.
  #
  # *Verb:* GET
  #
  # @accepts HTML
  def index
  end

end
