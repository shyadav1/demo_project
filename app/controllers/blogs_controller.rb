class BlogsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_blog, :redirect_if_unathorized!, only: %i[show edit update destroy]

  # Renders the User blogs.
  def index
    @blogs = current_user.blogs
  end

  def show
  end

  # Renders the new blog form.
  #
  # *Verb:* GET
  #
  # @accepts HTML
  def new
    @blog = current_user.blogs.new
  end

  def edit
  end

  # Creates a new blog for user.
  #
  # *Verb:* POST
  #
  # @post_params params [Hash]
  #  * :title [String]
  #  * :body [String]
  #  * :user_id [String]
  #
  # @accepts HTML, JSON
  def create
    @blog = current_user.blogs.new(blog_params)

    if @blog.save
      flash[:notice] = 'Blog was successfully created.'
      redirect_to user_blog_path(current_user, @blog)
    else
      flash[:notice] = 'Please try again later.'
      render :new, status: :unprocessable_entity
    end
  end

  # Updates the blog for user.
  #
  # *Verb:* POST
  #
  # @post_params params [Hash]
  #  * :title [String]
  #  * :body [String]
  #  * :user_id [String]
  #
  # @accepts HTML, JSON
  def update
    if @blog.update_attributes(blog_params)
      flash[:notice] = 'Blog was successfully updated.'
      redirect_to user_blog_path(current_user, @blog)
    else
      flash[:notice] = 'Please try again later.'
      render :edit, status: :unprocessable_entity
    end
  end

  # Destroy user blogs.
  #
  # *Verb:* DELETE
  #
  # @accepts HTML, JSON
  def destroy
    if @blog.destroy
      flash[:notice] = 'Blog was successfully destroyed.'
      redirect_to user_blogs_path(current_user)
    else
      flash[:notice] = 'Please try again later.'
      render :index
    end
  end

  private

  def set_blog
    @blog = Blog.find_by(id: params[:id]) if params[:id].present?
  end

  # Redirect the user if the Blog is not present or belongs to other user.
  def redirect_if_unathorized!
    return if @blog.present? && @blog.user == current_user

    flash[:notice] = 'Unathorized Access.'
    redirect_to user_blogs_path(current_user)
  end

  # Whitelisted parameters for Blog.
  def blog_params
    params.require(:blog).permit(
      :title, :body
    )
  end

end
