class UsersController < ApplicationController

  before_action :authenticate_user!

  # Renders <tt>User</tt> details page.
  #
  # *Verb:* Get
  #
  # @get_params params [Hash]
  #   :id [String]
  def show
  end

end
